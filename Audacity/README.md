Notes for Audacity
==================

The setup script downloads and builds everything in the directory `~/Source/Audacity/audacity`. All the instructions below assume you are in that directory.

NOTE: VirtualBox 5.0 appears to have a bug that prevents you from using the microphone, at least on a Mac. You may have better results using [VirtualBox 4.3.30](https://www.virtualbox.org/wiki/Download_Old_Builds_4_3).

Running
-------

Building produces a binary in the top-level directory of the source tree, so you should be able to run using:

    ./audacity &

To play/record sound inside a VM, set your playback device to "Intel 82801AA-ICH: - (hw:0,0)" and recording device to "Intel 82801AA-ICH: - (hw:0,0): Mic:0".

Your first "improvement"
------------------------

Audacity uses wxWidgets, so it doesn't have a `main()` function. You
can put your first "improvement" inside `AudacityApp::OnInit()`.

Try adding a `printf("Go Irish!\n");` to that function, and rebuild:

    make

More about building
-------------------

If you modify `#includes` in files:

    make dep; make

If you modify `configure.ac`, `Makefile.am`, or any files in `m4/`:

    autoreconf; ./configure; make dep; make

If you add or remove files:

    ./configure; make dep; make
