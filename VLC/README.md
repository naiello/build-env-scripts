Notes for VLC media player
==========================

The setup script downloads and builds everything in the directory `~/Source/VLC/vlc`. All the instructions below assume you are in that directory.

Running
-------

Building produces a binary in the top-level directory of the source tree, so you should be able to run using:

    ./vlc &

Your first "improvement"
------------------------

The `main()` function is located inside `bin/vlc.c`.

Try adding a `printf("Go Irish!\n");` to that function, and rebuild:

    make

More about building
-------------------

If you change any dependencies, you may need to run:

    ./bootstrap; ./configure

before rebuilding.
